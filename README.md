Usage 

    Save a text file of your decklist as 'decklist.txt'. The program will import the mana base from it. The 'decklist.txt' here is an example.
    Currently don't have an interface. Working on that.

Assumptions

    1. Drawing one card a turn
    
    2. No London mulligans. Currently just does the Paris
    
    3. Little manipulation ex: scry lands, drawing additional cards
        Fetches and stuff like Cultivate and Explore are supported
    
    4. Currently available mana is calculated before playing mana rocks. This means that a turn 1 mana crypt won't show 3 available mana.

Suggest me things. I'm a little new to the stuff so you have features you want to see or things I could implement better let me know.

Also yes I know the README looks garbage. Currently learning new things.
