class Card:
    def __init__(self, card_type, generic_cost, green_cost, generic_amount, green_amount, enters_tapped, cards_to_hand, is_fetch, is_explore_permanent, lands_sacrificed, cards_to_activate):
        self.card_type = card_type
        self.generic_cost = generic_cost
        self.green_cost = green_cost
        self.generic_amount = generic_amount
        self.green_amount = green_amount
        self.enters_tapped = enters_tapped
        self.cards_to_hand = cards_to_hand
        self.is_fetch = is_fetch
        self.is_explore_permanent = is_explore_permanent
        self.is_tapped = enters_tapped
        self.lands_sacrificed = lands_sacrificed
        self.cards_to_activate = cards_to_activate