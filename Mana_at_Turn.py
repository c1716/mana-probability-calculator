import random
import matplotlib.pyplot as plt
import copy
from card_list import card_database
import csv
#Add cards to deck
def add_to_deck(card, deck, amount):
    for i in range(0, amount):
        if amount > 1:
            deck.append(copy.deepcopy(card))
        else:
            deck.append(card)
    return deck
#Draw a new card
def draw_card(deck, amount, hand):
    for i in range(0, amount):
        hand.append(deck[0])
        deck.pop(0)
    return (deck, hand)
#Sort hand from greatest to least in the following priority: 
#By net mana for turn, net for next turn, enters tapped, adds cards to hand, is a fetch, requires other cards in play 
def sort_hand(hand):
    hand = sorted(hand, key=lambda card: (card.generic_amount + card.green_amount - card.generic_cost - card.green_cost, card.generic_amount + card.green_cost, not card.enters_tapped, card.is_explore_permanent, card.cards_to_hand, card.is_fetch, -1 * card.cards_to_activate), reverse=True)
    return hand
#Play a land
def play_land(hand, in_play, deck, num_land_drops):
    lands_in_play = count_lands(in_play)
    lands_in_hand = count_lands(hand)
    
    for i in range(0, num_land_drops):
        for card in hand:
            if card.card_type == "Land":
                if lands_in_play < card.cards_to_activate-1:
                    if lands_in_hand > 1:
                        continue
                    else:
                        in_play.append(card)
                        hand.pop(hand.index(card))
                    
                    
                elif card.is_fetch:
                    in_play, hand, deck = tutor_land(in_play, hand, deck, card.generic_amount, card.cards_to_hand, card.enters_tapped)
                    hand.pop(hand.index(card))
                    if card.enters_tapped:
                        in_play[-1].is_tapped = True
                else:
                    in_play.append(card)
                    hand.pop(hand.index(card))
                num_land_drops -= 1
                break        
    return hand, in_play, deck, num_land_drops
#Tutors a land to hand and/or play               
def tutor_land(in_play, hand, deck, generic_amount, amount_hand, enters_tapped):
    for i in range(0, generic_amount):
        for card in deck:
            if (card.card_type == "Land") and (card.generic_amount + card.green_amount == 1) and (not card.is_fetch) and (not card.enters_tapped):
                in_play.append(card)
                if enters_tapped:
                    in_play[-1].is_tapped = True
                deck.pop(deck.index(card))
                break
    for i in range(0, amount_hand):
        for card in deck:
            if card.card_type == "Land" and (card.generic_amount + card.green_amount == 1) and (not card.is_fetch):
                hand.append(card)
                deck.pop(deck.index(card))
                break                              
    return in_play, hand, deck               
#Plays cards until it runs out of mana                                        
def play_cards(hand, in_play, deck, num_land_drops, mana):
    i = 0
    while (i < len(hand)):
        if ((hand[i].generic_cost + hand[i].green_cost) <= mana) and (hand[i].card_type != 'Land') and (hand[i].card_type != 'Junk'):
            if hand[i].card_type == 'Rock':
                hand, in_play = play_rock(hand[i], hand, in_play)
            elif hand[i].card_type == 'Spell':
                hand, in_play, deck = play_spell(hand[i], hand, in_play, deck)
            elif hand[i].card_type == 'Explore':
                hand, in_play, deck, num_land_drops = play_explore(hand[i], hand, in_play, deck, num_land_drops)
            i -= 1  
            hand = sort_hand(hand)
            in_play = sort_mana(in_play)
            hand, in_play, deck, num_land_drops = play_land(hand, in_play, deck, num_land_drops)
            in_play = sort_mana(in_play)
            mana = count_mana(in_play)
        i += 1
    return hand, in_play, deck, num_land_drops  
#Plays a mana rock        
def play_rock(card, hand, in_play):
    generic_paid = 0
    green_paid = 0
    for mana_source in in_play:
        if not mana_source.is_tapped:
            if card.green_cost > green_paid:
                green_paid += mana_source.green_amount
                generic_paid += mana_source.generic_amount   
                mana_source.is_tapped = True
            if card.generic_cost > generic_paid:
                generic_paid += mana_source.generic_amount + mana_source.green_amount
                mana_source.is_tapped = True
    in_play.append(card)
    hand.pop(hand.index(card))        
    return hand, in_play
#Plays a ramp spell
def play_spell(card, hand, in_play, deck):
    generic_paid = 0
    green_paid = 0
    for mana_source in in_play:
        if (not mana_source.is_tapped):
            if (card.green_cost > green_paid) and (mana_source.green_amount > 0):
                green_paid += mana_source.green_amount
                generic_paid += mana_source.generic_amount   
                mana_source.is_tapped = True
            if card.generic_cost > generic_paid:
                generic_paid += mana_source.generic_amount + mana_source.green_amount
                mana_source.is_tapped = True
    if (generic_paid == card.generic_cost) and (green_paid == card.green_cost):    
        for i in range(0, card.lands_sacrificed):
            for mana_source in in_play:
                if mana_source.card_type == 'Land':
                    in_play.pop(in_play.index(mana_source))
                    break       
        in_play, hand, deck = tutor_land(in_play, hand, deck, card.generic_amount, card.cards_to_hand, card.enters_tapped)
        hand.pop(hand.index(card))  
        if card.enters_tapped:
            in_play[-1].is_tapped = True
    return hand, in_play, deck
#Plays exploration / explore effects
def play_explore(card, hand, in_play, deck, num_land_drops):
    generic_paid = 0
    green_paid = 0
    for mana_source in in_play:
        if not mana_source.is_tapped:
            if card.green_cost > green_paid:
                green_paid += mana_source.green_amount
                generic_paid += mana_source.generic_amount   
                mana_source.is_tapped = True
            if card.generic_cost > generic_paid:
                generic_paid += mana_source.generic_amount + mana_source.green_amount
                mana_source.is_tapped = Trueland_count = count_lands(in_play)
    if card.is_explore_permanent:
        in_play.append(card)
    hand.pop(hand.index(card)) 
    num_land_drops += card.generic_amount
    deck, hand = draw_card(deck, card.cards_to_hand, hand)
    return hand, in_play, deck, num_land_drops
def count_lands(l):
    cnt = 0
    for card in l:
        if (card.card_type == 'Land'):
            cnt += 1
    return cnt
#Tallies up mana from untapped lands/rocks
def count_mana(in_play):
    mana = 0
    land_count = count_lands(in_play)
    for card in in_play:
        if (not card.is_tapped) and (not card.is_explore_permanent) and (land_count >= card.cards_to_activate):
            mana += card.generic_amount + card.green_amount
    return mana
#Sort mana from least to greatest in the following priority: 
#By whether or not its exploration, the amount of green it produces, and the amount of non-green it produces
def sort_mana(in_play):
    in_play = sorted(in_play, key=lambda card: (card.is_explore_permanent, card.green_amount, card.generic_amount))
    return in_play
#Plays through a single turn
def take_turn(hand, in_play, deck, num_land_drops):
    lands_played = num_land_drops
    deck, hand = draw_card(deck, 1, hand)
    hand = sort_hand(hand)
    in_play = sort_mana(in_play)
    hand, in_play, deck, num_land_drops = play_land(hand, in_play, deck, num_land_drops)
    mana = count_mana(in_play)
    mana_after_land = mana 
    lands_played -= num_land_drops
    hand, in_play, deck, num_land_drops = play_cards(hand, in_play, deck, num_land_drops, mana)
    mana = count_mana(in_play)
    if mana > mana_after_land:
        mana_after_land = mana
    return hand, in_play, deck, mana_after_land, lands_played
#Evaluates whether or not to mulligan. Takes one if needed    
def mulligan(hand, deck, times_mulliganed, original_deck, free_mull):
    if len(hand) == 2:
        return hand, deck
    if times_mulliganed >= 3:
        land_count = 0
        for card in hand:
            if card.card_type == 'Land':
                land_count += 1
                if card.cards_to_activate > 0:
                    land_count -= 1
                if land_count >= 1:
                    return hand, deck
                else:
                    hand, deck = take_mull(times_mulliganed, original_deck, free_mull)
    land_count = 0
    ramp_count = 0
    green_sources = 0
    green_costs = 0
    for card in hand:
        if card.card_type == 'Land':
            land_count += 1
            if card.cards_to_activate > 0:
                land_count -= 1
        if (card.card_type != 'Junk') and (card.card_type != 'Land'):
            ramp_count += 1
        if card.green_amount > 0:
            green_sources += card.green_amount
        if card.green_cost > 0:
            green_costs += card.green_cost
    if 2 <= land_count <= 4:
        if 3 <= ramp_count + land_count <= 5:
            if (green_costs > 0) and (green_sources > 0):
                return hand, deck
            elif green_costs == 0:
                return hand, deck
            else:
                hand, deck = take_mull(times_mulliganed, original_deck, free_mull)
        else:
            hand, deck = take_mull(times_mulliganed, original_deck, free_mull)
    else:
        hand, deck = take_mull(times_mulliganed, original_deck, free_mull)
    return hand, deck

def take_mull(times_mulliganed, original_deck, free_mull):
    random.shuffle(original_deck)
    deck = []
    for card in original_deck:
            deck.append(card)
    if free_mull:
        times_mulliganed -= 1
    deck, hand = draw_card(deck, 7-(times_mulliganed + 1), [])
    hand, deck = mulligan(hand, deck, times_mulliganed + 1, original_deck, False)
    
    return hand, deck

num_trials = 10000
num_turns = 10
mana_results = [0]*num_turns
land_results = [0]*num_turns
avg_hand_size = 0
free_mull = True
deck_size = 99
lands_found = 0
ramp_found = 0

#Extract decklist from .txt file
with open('decklist.txt', 'r') as file:
    data = file.read().splitlines()
    for line in data:
        word = line.split(' ',1)
        #print(word)
        if word[1] in card_database:
            print('Found %s %s' % (word[0], word[1]))
            card_database[word[1]][1] = int(word[0])
            if card_database[word[1]][0].card_type == 'Land':
                lands_found += int(word[0])
            elif card_database[word[1]][0].card_type != 'Junk':
                ramp_found += int(word[0])
            deck_size -= int(word[0])
card_database['Junk'][1] = deck_size
print('%s lands found' % lands_found)
print('%s ramp found' % ramp_found)
print('%s non-mana cards found' % deck_size)        

for i in range(0, num_trials):  
    deck = []
    original_deck = []
    in_play = []
    #Add cards to the deck
    for key in card_database:
        deck = add_to_deck(card_database[key][0], deck, card_database[key][1])
    random.shuffle(deck)
    for i in range(0, len(deck)):
        original_deck.append(deck[i])
    #Draw opening hand and evaluate whether or not to mulligan
    deck, hand = draw_card(deck, 7, [])
    hand, deck = mulligan(hand, deck, 0, original_deck, free_mull)
    avg_hand_size += len(hand)
    #Play for specified number of turns
    for j in range(0, num_turns):
        num_land_drops = 1
        for card in in_play:
            card.is_tapped = False
            #Adds additional land drops from exploration
            if (card.card_type == 'Explore') and (card.is_explore_permanent):
                num_land_drops += card.generic_amount
        hand, in_play, deck, mana_after_land, lands_played = take_turn(hand, in_play, deck, num_land_drops)
        #Stores mana amount for graphing later
        mana_results[j] += mana_after_land
        land_results[j] += lands_played
#Averages the mana per turn        
for res in mana_results:
    mana_results[mana_results.index(res)] = res / num_trials
for res in land_results:
    land_results[land_results.index(res)] = res / num_trials   
for i in range(1, len(land_results)):
    land_results[i] += land_results[i-1]
    
print('Average opening hand size is %f' % (avg_hand_size / num_trials))
#Plots the results     
plt.stem(list(range(1, num_turns+1)), mana_results, label='Average mana')
plt.stem(list(range(1, num_turns+1)), land_results, markerfmt="C1o", label='Average lands played')
plt.legend()
plt.xlabel('Number of Turns')
plt.xticks(range(1, num_turns+1))
plt.ylabel('Average')
for i in range(0, num_turns):
    plt.annotate(str('Mana: %g' % mana_results[i]), (i+1, mana_results[i]), xytext=(0, 20), textcoords='offset points', ha='center')
    plt.annotate(str('Lands: %g' % land_results[i]), (i+1, land_results[i]), xytext=(0, -20), textcoords='offset points', ha='center')
plt.show()        
           
                