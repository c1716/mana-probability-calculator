from card_class import Card

card_database = {
        #Non Lands
        'Junk':                         [Card('Junk'   , 0, 0, 0, 0, True , 0, False, False, 0, 0), 0],                    
        #Lands
        'Plains':                       [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Mountain':                     [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Island':                       [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Swamp':                        [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Forest':                       [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Snow-Covered Plains':          [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Snow-Covered Mountain':        [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Snow-Covered Island':          [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Snow-Covered Swamp':           [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Snow-Covered Forest':          [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Command Tower':                [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Evolving Wilds':               [Card('Land'   , 0, 0, 1, 0, True , 0, True , False, 0, 0), 0],
        'Reliquary Tower':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Exotic Orchard':               [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed opponent has green land
        'Terramorphic Expanse':         [Card('Land'   , 0, 0, 1, 0, True , 0, True , False, 0, 0), 0],
        'Myriad Landscape':             [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Rogue\'s Passage':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Bojuka Bog':                   [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Polluted Delta':               [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Windswept Heath':              [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Flooded Strand':               [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Watery Grave':                 [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Ancient Tomb':                 [Card('Land'   , 0, 0, 2, 0, False, 0, False, False, 0, 0), 0],
        'Wooded Foothills':             [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Overgrown Tomb':               [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Bloodstained Mire':            [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Breeding Pool':                [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Fabled Passage':               [Card('Land'   , 0, 0, 1, 0, True , 0, True , False, 0, 0), 0], #Assumed always enters tapped
        'Misty Rainforest':             [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Steam Vents':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Godless Shrine':               [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Hallowed Fountain':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Urborg, Tomb of Yawgmoth':     [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Verdant Catacombs':            [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Marsh Flats':                  [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Stomping Ground':              [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Blood Crypt':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Temple Garden':                [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Scalding Tarn':                [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Sacred Foundry':               [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Arid Mesa':                    [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Mana Confluence':              [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Prismatic Vista':              [Card('Land'   , 0, 0, 1, 0, False, 0, True , False, 0, 0), 0],
        'Path of Ancestry':             [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'City of Brass':                [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Nykthos, Shrine to Nyx':       [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed only taps for 1
        'Hinterland Harbor':            [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Drowned Catacomb':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Dragonskull Summit':           [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Woodland Cemetery':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Yavimaya Coast':               [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Sulfur Falls':                 [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Isolated Chapel':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Llanowar Wastes':              [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Buried Ruin':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Cinder Glade':                 [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Strip Mine':                   [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Sunken Hollow':                [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Blighted Woodland':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed won't be activated
        'Clifftop Retreat':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Sunpetal Grove':               [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Shivan Reef':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Glacial Fortress':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed enters untapped
        'Ash Barrens':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Asusmed won't be cycled
        'Caves of Koilos':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Rootbound Crag':               [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Inventors\' Fair':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Prairie Stream':               [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Reflecting Pool':              [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumes green source available
        'Ghost Quarter':                [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Temple of Mystery':            [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0], #Assumes no scry
        'Canopy Vista':                 [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Cavern of Souls':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Morphic Pool':                 [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Field of the Dead':            [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Battlefield Forge':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Temple of Silence':            [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0], #Assumes no scry
        'Scavenger Grounds':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Temple of Malady':             [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0], #Assumes no scry
        'Darksteel Citadel':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Smoldering Marsh':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Temple of Triumph':            [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0], #Assumes no scry
        'Castle Garenbrig':             [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Dryad Arbor':                  [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0],
        'Game Trail':                   [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Mosswort Bridge':              [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0], #Assumes no Hideaway
        'Sheltered Thicket':            [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0],
        'Valakut, the Molten Pinnacle': [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Cabal Stronghold':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Hagra Mauling':                [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Karn\'s Bastion':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Terrain Generator':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes no activated ability
        'Thespian\'s Stage':            [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Memorial to Folly':            [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Treetop Village':              [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0],
        'Khalni Garden':                [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0],
        'Spawning Pool':                [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Geier Reach Sanitarium':       [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Dakmor Salvage':               [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Tainted Wood':                 [Card('Land'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumes swamp in play
        'Hissing Quagmire':             [Card('Land'   , 0, 0, 0, 1, True , 0, False, False, 0, 0), 0],
        'Academy Ruins':                [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Castle Vantress':              [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Mikokoro, Center of the Sea':  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Mystic Sanctuary':             [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0], #Assumes enters tapped
        'Sea Gate Restoration':         [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Ancient Den':                  [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Castle Ardenvale':             [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes enters untapped
        'Emeria, the Sky Ruin':         [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Ondu Inversion':               [Card('Land'   , 0, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Treasure Vault':               [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'War Room':                     [Card('Land'   , 0, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Temple of the False God':      [Card('Land'   , 0, 0, 2, 0, False, 0, False, False, 0, 5), 0],
        #Mana Rock  
        'Sol Ring':                     [Card('Rock'   , 1, 0, 2, 0, False, 0, False, False, 0, 0), 0],
        'Arcane Signet':                [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Mind Stone':                   [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Commander\'s Sphere':          [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Fellwar Stone':                [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Mana Crypt':                   [Card('Rock'   , 0, 0, 2, 0, False, 0, False, False, 0, 0), 0],
        'Chromatic Lantern':            [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Thought Vessel':               [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Izzet Signet':                 [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Dimir Signet':                 [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Chrome Mox':                   [Card('Rock'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed to be a Mox Emerald
        'Orzhov Signet':                [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Boros Signet':                 [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Rakdos Signet':                [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Azorius Signet':               [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Gilded Lotus':                 [Card('Rock'   , 5, 0, 0, 3, False, 0, False, False, 0, 0), 0],
        'Thran Dynamo':                 [Card('Rock'   , 4, 0, 3, 0, False, 0, False, False, 0, 0), 0],
        'Talisman of Creativity':       [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Simic Signet':                 [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Everflowing Chalice':          [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed kicked once
        'Mox Diamond':                  [Card('Rock'   , 0, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed to be a Mox Emerald
        'Hedron Archive':               [Card('Rock'   , 4, 0, 2, 0, False, 0, False, False, 0, 0), 0],
        'Talisman of Dominance':        [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Talisman of Hierarchy':        [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Talisman of Conviction':       [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Golgari Signet':               [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Worn Powerstone':              [Card('Rock'   , 3, 0, 2, 0, True , 0, False, False, 0, 0), 0],
        'Darksteel Ingot':              [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Talisman of Indulgence':       [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Prismatic Lens':               [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Gruul Signet':                 [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Selesnya Signet':              [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Fire Diamond':                 [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Talisman of Progress':         [Card('Rock'   , 2, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Talisman of Curiosity':        [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Sky Diamond':                  [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Charcoal Diamond':             [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Talisman of Resilience':       [Card('Rock'   , 2, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        'Heraldic Banner':              [Card('Rock'   , 3, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed chosen color is NOT green
        'Star Compass':                 [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0], #Assumed no basic forest in play
        'Marble Diamond':               [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Coldsteel Heart':              [Card('Rock'   , 2, 0, 1, 0, True , 0, False, False, 0, 0), 0], #Assumed chosed color is NOT green
        'Skyclave Relic':               [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed not kicked
        'Mana Geode':                   [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed no scry
        'Chromatic Orrery':             [Card('Rock'   , 7, 0, 0, 5, False, 0, False, False, 0, 0), 0],
        'Pristine Talisman':            [Card('Rock'   , 3, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Dreamstone Hedron':            [Card('Rock'   , 6, 0, 3, 0, False, 0, False, False, 0, 0), 0],
        'Coalition Relic':              [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0], #Assumed no charge counters
        'Unstable Obelisk':             [Card('Rock'   , 3, 0, 1, 0, False, 0, False, False, 0, 0), 0],
        'Midnight Clock':               [Card('Rock'   , 3, 0, 1, 0, False, 0, False, False, 0, 0), 0], #Assumes no wheel effect
        'Bonder\'s Ornament':           [Card('Rock'   , 3, 0, 0, 1, False, 0, False, False, 0, 0), 0],
        #Mana Dorks
        'Llanowar Elves':               [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        'Birds of Paradise':            [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        'Elvish Mystic':                [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        'Fyndhorn Elves':               [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        'Arbor Elf':                    [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0], #Assumed forest in play
        'Elves of Deep Shadow':         [Card('Rock'   , 0, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Avacyn\'s Pilgrim':            [Card('Rock'   , 0, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Boreal Druid':                 [Card('Rock'   , 0, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Noble Hierarch':               [Card('Rock'   , 0, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        'Palladium Myr':                [Card('Rock'   , 3, 0, 2, 0, True , 0, False, False, 0, 0), 0],
        'Skull Prophet':                [Card('Rock'   , 1, 1, 0, 1, True , 0, False, False, 0, 0), 0],
        #Ramp Spells
        'Cultivate':                    [Card('Spell'  , 2, 1, 1, 0, True , 1, False, False, 0, 0), 0],
        'Solemn Simulacrum':            [Card('Spell'  , 4, 0, 1, 0, True , 0, False, False, 0, 0), 0],
        'Kodama\'s Reach':              [Card('Spell'  , 2, 1, 1, 0, True , 1, False, False, 0, 0), 0],
        'Rampant Growth':               [Card('Spell'  , 1, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Farseek':                      [Card('Spell'  , 1, 1, 1, 0, False, 0, False, False, 0, 0), 0],
        'Sakura-Tribe Elder':           [Card('Spell'  , 1, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Nature\'s Lore':               [Card('Spell'  , 1, 1, 1, 0, False, 0, False, False, 0, 0), 0],
        'Skyshroud Claim':              [Card('Spell'  , 3, 1, 2, 0, False, 0, False, False, 0, 0), 0],
        'Three Visits':                 [Card('Spell'  , 1, 1, 1, 0, False, 0, False, False, 0, 0), 0],
        'Wood Elves':                   [Card('Spell'  , 2, 1, 1, 0, False, 0, False, False, 0, 0), 0],
        'Explosive Vegetation':         [Card('Spell'  , 3, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Farhaven Elf':                 [Card('Spell'  , 2, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Circuitous Route':             [Card('Spell'  , 3, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Migration Path':               [Card('Spell'  , 3, 1, 1, 0, True , 0, False, False, 0, 0), 0],
        'Search for Tomorrow':          [Card('Spell'  , 2, 1, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed NOT suspended
        'Grow from the Ashes':          [Card('Spell'  , 2, 1, 1, 0, False, 0, False, False, 0, 0), 0], #Assumed NOT kicked
        'Beanstalk Giant':              [Card('Spell'  , 2, 1, 1, 0, False, 0, False, False, 0, 0), 0],
        'Avatar of Growth':             [Card('Spell'  , 1, 2, 2, 0, False, 0, False, False, 0, 0), 0], #Assumed 3 opponents
        'Nissa, Vastwood Seer':         [Card('Spell'  , 2, 1, 0, 0, False, 1, False, False, 0, 0), 0],
        'Archaeomancer\'s Map':         [Card('Spell'  , 3, 0, 0, 0, False, 2, False, False, 0, 0), 0], #Only puts lands in hand
        'Harrow':                       [Card('Spell'  , 2, 1, 2, 0, False, 0, False, False, 1, 0), 0],
        'Springbloom Druid':            [Card('Spell'  , 2, 1, 2, 0, True , 0, False, False, 1, 0), 0],
        #To add: Springbloom Druid
        #Explore Effects
        'Exploration':                  [Card('Explore', 0, 1, 1, 0, False, 0, False, True , 0, 0), 0],
        'Explore':                      [Card('Explore', 1, 1, 1, 0, False, 1, False, False, 0, 0), 0],
        'Growth Spiral':                [Card('Explore', 1, 1, 1, 0, False, 1, False, False, 0, 0), 0],
        'Azusa, Lost but Seeking':      [Card('Explore', 2, 1, 2, 0, False, 0, False, True , 0, 0), 0],
        'Dryad of the Ilysian Grove':   [Card('Explore', 2, 1, 1, 0, False, 0, False, True , 0, 0), 0],
        'Mina and Denn, Wildborn':      [Card('Explore', 3, 1, 1, 0, False, 0, False, True , 0, 0), 0]   
    }




